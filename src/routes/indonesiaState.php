<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->get('/api/indonesia/', function(Request $request, Response $response){
    $sql = "SELECT * FROM provinces";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customers = $stmt->fetchAll(PDO::FETCH_OBJ);

        $db = null;
        
        $someArray = [];

        foreach ($customers as $result) {
            //$rs  = array('id' => $result->id,'name' =>$result->name);
            array_push($someArray, [
              'id_provinsi'   => $result->id,
              'nama_provinsi' => ucwords(strtolower($result->name))
            ]);
        } 

        echo json_encode($someArray);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->get('/api/indonesia/{prov}', function(Request $request, Response $response){
    $prov = $request->getAttribute('prov');

    $sql = "SELECT * FROM regencies WHERE province_id = $prov";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customers = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if(count($customers)>0)
        {
            $someArray = [];

            foreach ($customers as $result) {
                //$rs  = array('id' => $result->id,'name' =>$result->name);
                array_push($someArray, [
                  'id_provinsi' => $prov,
                  'id_kabupaten'   => $result->id,
                  'nama_kecamatan' => ucwords(strtolower($result->name))
                ]);
            } 

        }else {
            $someArray = ['id'=>'','name'=>''];
        }
        
        echo json_encode($someArray);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});

$app->get('/api/indonesia/{prov}/{kab}', function(Request $request, Response $response){
    $prov = $request->getAttribute('prov');
    $kab = $request->getAttribute('kab');

    $sql = "SELECT * FROM districts WHERE regency_id = $kab";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customers = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        if(count($customers)>0)
        {
            $someArray = [];

            foreach ($customers as $result) {
                //$rs  = array('id' => $result->id,'name' =>$result->name);
                array_push($someArray, [
                  'id_provinsi' => $prov,
                  'id_kabupaten'   => $kab,
                  'id_kecamatan'   => $result->id,
                  'nama_kecamatan' => ucwords(strtolower($result->name))
                ]);
            } 

        }else {
            $someArray = ['id'=>'','name'=>''];
        }
        
        echo json_encode($someArray);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }
});