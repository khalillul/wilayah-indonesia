<?php
require 'src/config/db.php';

$sql = "SELECT * FROM provinces";

    try{
        // Get DB Object
        $db = new db();
        // Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $customers = $stmt->fetchAll(PDO::FETCH_OBJ);
        
        $db = null;

        $someArray = [];

        foreach ($customers as $result) {
        	//$rs  = array('id' => $result->id,'name' =>$result->name);
        	array_push($someArray, [
		      'id'   => $result->id,
		      'name' => ucwords(strtolower($result->name))
		    ]);
		} 

        echo json_encode($someArray);

    } catch(PDOException $e){
        echo '{"error": {"text": '.$e->getMessage().'}';
    }